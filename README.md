Desenvolvimento de APIs REST
==============================================================================================
Author: Raphael A Militani


Sobre
-----------

Criação de duas APIs REST para clientes se cadastrarem em campanhas e criarem seu cadastro de sócio torcedor.


Requisitos do Sistema
-------------------

Para o build do projeto é necessário Java 8.0 (Java SDK 1.8) ou mais recente, Maven 3.1 ou mais recente.

Além disso, o projeto foi executado no Servidor de Aplicação WildFly 9.

Para o banco de dados foi utilizado o MySQL.


Arquitetura do Sistema
-----------------------

O sistema foi construído sobre a plataforma JAVAEE, e modularizado através do MAVEN. 

Na camada web, para a criação do Webservice Restful foi utilizado JAX-RS pela implementação RESTEasy.

Na camada de negócios foi utilizado EJB Stateless para tratamento das requisições, e gerenciamento de transações.

Na camada DAO foi utilzado JPA com a implementação por Hibernate.

O banco de dados utilizado foi o MYSQL.


Observação para o deploy
--------------------------

Para o build correto, no POM do projeto desafio-ejb deve-se alterar o valor do "systemPath" com o caminho da sua JDK local.

<dependency>
 
 <groupId>sun.jdk</groupId>
 
 <artifactId>jconsole</artifactId>
 
 <version>1.8</version>
 
 <scope>system</scope>
 
 <systemPath>C:\Program Files\Java\jdk1.8.0_91\lib\jconsole.jar</systemPath>

</dependency>

---------------------------------------------------------------------------------------------------------------------
Links para acesso as APIs REST
---------------------

URL para busca de campanhas:
----------------------------
http://localhost:8080/desafio/rest/campanhas/ {id da campanha}

Método HTTP: GET

URL de exemplo: http://localhost:8080/desafio/rest/campanhas/41

URL para criação de campanhas:
------------------------------

URL: http://localhost:8080/desafio/rest/campanhas
 
Método HTTP: POST 
 
JSON de exemplo: { "nome": "Campanha 1", "idTime": 1, "dataInicioVigencia": "01/10/2017", "dataFimVigencia":"03/10/2017"}

URL para atualização de campanhas:
-------------------------------

URL: http://localhost:8080/desafio/rest/campanhas

Método HTTP: PUT 

JSON de exemplo: { "id":48, "nome": "Campanha Atualizacao teste", "idTime": 1,  "dataInicioVigencia": "01/10/2017",  "dataFimVigencia":"05/10/2017"}

URL para remoção de campanhas:
-------------------------------

URL: http://localhost:8080/desafio/rest/campanhas/ {id da campanha}

Método HTTP: DELETE

URL de exemplo: http://localhost:8080/desafio/rest/campanhas/41
 
 
URL para cadastro de sócio torcedor:
------------------------------------

URL: http://localhost:8080/desafio/rest/sociotorcedor

Método HTTP: POST

JSON de exemplo: {"nome": "Socio Torcedor 1", "email" : "teste@gmail.com", "dataNascimento": "01/10/2017", "idTime": 1}



