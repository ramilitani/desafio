package br.com.appsoftware.desafio.rest.test;

import static org.junit.Assert.fail;

import org.junit.Assert;
import org.junit.Test;

import br.com.appsoftware.desafio.dto.CampanhaDTO;
import br.com.appsoftware.desafio.exception.DesafioException;
import br.com.appsoftware.desafio.rest.CampanhasRESTService;

public class CampanhaTestSemIntegracao {
	
	@Test()
	public void cadastraCampanhaNula(){
		CampanhasRESTService service = new CampanhasRESTService();
		try {
			service.incluiCampanha(null);
			fail("Deveria retornar exception");
		} catch (DesafioException e) {
			Assert.assertEquals(e.getMessage(), "Falha ao cadastrar a campanha. Campanha não preenchida");
		}
	}
	
	@Test()
	public void cadastraCampanhaSemNome() {
		CampanhasRESTService service = new CampanhasRESTService();
		CampanhaDTO campanhaDTO = new CampanhaDTO();
		
		try {
			service.incluiCampanha(campanhaDTO);
			fail("Deveria retornar exception");
		} catch (DesafioException e) {
			Assert.assertEquals(e.getMessage(), "Falha ao cadastrar a campanha. Nome não preenchido");
		}
	}
	
	@Test()
	public void cadastraCamapanhaSemIdTime() {
		CampanhasRESTService service = new CampanhasRESTService();
		CampanhaDTO campanhaDTO = new CampanhaDTO();
		campanhaDTO.setNome("Campanha teste 1");
		
		try {
			service.incluiCampanha(campanhaDTO);
			fail("Deveria retornar exception");
		} catch (DesafioException e) {
			Assert.assertEquals(e.getMessage(), "Falha ao cadastrar a campanha. Id do time não preenchido");
		}
	}
	
	@Test()
	public void cadastraCampanhaSemDataInicial() {
		CampanhasRESTService service = new CampanhasRESTService();
		CampanhaDTO campanhaDTO = new CampanhaDTO();
		campanhaDTO.setNome("Campanha teste 1");
		campanhaDTO.setIdTime(new Long(1));
		
		try {
			service.incluiCampanha(campanhaDTO);
			fail("Deveria retornar exception");
		} catch (DesafioException e) {
			Assert.assertEquals(e.getMessage(), "Falha ao cadastrar a campanha. Data inicial da vigência não preenchida");
		}
	}
	
	@Test()
	public void cadastraCampanhaSemDataFinal() {
		CampanhasRESTService service = new CampanhasRESTService();
		CampanhaDTO campanhaDTO = new CampanhaDTO();
		campanhaDTO.setNome("Campanha teste 1");
		campanhaDTO.setIdTime(new Long(1));
		campanhaDTO.setDataInicioVigencia("04/05/2017");
		
		try {
			service.incluiCampanha(campanhaDTO);
			fail("Deveria retornar exception");
		} catch (DesafioException e) {
			Assert.assertEquals(e.getMessage(), "Falha ao cadastrar a campanha. Data final da vigência não preenchida");
		}
	}
	
	@Test()
	public void cadastraCampanhaComDataInvalida() {
		CampanhasRESTService service = new CampanhasRESTService();
		CampanhaDTO campanhaDTO = new CampanhaDTO();
		campanhaDTO.setNome("Campanha teste 1");
		campanhaDTO.setIdTime(new Long(1));
		campanhaDTO.setDataInicioVigencia("22");
		campanhaDTO.setDataFimVigencia("04/05/2017");
		
		try {
			service.incluiCampanha(campanhaDTO);
			fail("Deveria retornar exception");
		} catch (DesafioException e) {
			Assert.assertEquals(e.getMessage(), "Falha ao cadastrar a campanha. Formato de data inválido. ex: 10/10/2016");
		}
	}
	
	@Test()
	public void cadastraCampanhaComDataInicialAnteriorCorrente() {
		CampanhasRESTService service = new CampanhasRESTService();
		CampanhaDTO campanhaDTO = new CampanhaDTO();
		campanhaDTO.setNome("Campanha teste 1");
		campanhaDTO.setIdTime(new Long(1));
		campanhaDTO.setDataInicioVigencia("05/05/2017");
		campanhaDTO.setDataFimVigencia("06/05/2017");
		
		try {
			service.incluiCampanha(campanhaDTO);
			fail("Deveria retornar exception");
		} catch (DesafioException e) {
			Assert.assertEquals(e.getMessage(), "Falha ao cadastrar a campanha. Data inicial menor que a data corrente");
		}
	}
	
	@Test()
	public void cadastraCampanhaComDataInicialMaiorQueDataFinal() {
		CampanhasRESTService service = new CampanhasRESTService();
		CampanhaDTO campanhaDTO = new CampanhaDTO();
		campanhaDTO.setNome("Campanha teste 1");
		campanhaDTO.setIdTime(new Long(1));
		campanhaDTO.setDataInicioVigencia("08/05/2017");
		campanhaDTO.setDataFimVigencia("07/05/2017");
		
		try {
			service.incluiCampanha(campanhaDTO);
			fail("Deveria retornar exception");
		} catch(DesafioException e) {
			Assert.assertEquals(e.getMessage(), "Falha ao cadastrar a campanha. Data inicial maior que data final");
		}
	}
}
