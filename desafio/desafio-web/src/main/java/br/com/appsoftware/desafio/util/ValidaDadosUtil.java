package br.com.appsoftware.desafio.util;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;

import org.apache.commons.lang3.StringUtils;

import br.com.appsoftware.desafio.dto.CampanhaDTO;
import br.com.appsoftware.desafio.dto.SocioTorcedorDTO;
import br.com.appsoftware.desafio.exception.DesafioException;

public final class ValidaDadosUtil {
	
	public static void validaDadosEntradaCampanha(CampanhaDTO campanhaDTO) throws DesafioException {
		
		if(campanhaDTO == null)
			throw new DesafioException("Falha ao cadastrar a campanha. Campanha não preenchida");
		
		if(StringUtils.isBlank(campanhaDTO.getNome()))
			throw new DesafioException("Falha ao cadastrar a campanha. Nome não preenchido");
		
		if(campanhaDTO.getIdTime() == null)
			throw new DesafioException("Falha ao cadastrar a campanha. Id do time não preenchido");
		
		if(StringUtils.isBlank(campanhaDTO.getDataInicioVigencia()))
			throw new DesafioException("Falha ao cadastrar a campanha. Data inicial da vigência não preenchida");
		
		if(StringUtils.isBlank(campanhaDTO.getDataFimVigencia()))
			throw new DesafioException("Falha ao cadastrar a campanha. Data final da vigência não preenchida");
		
		try {
			LocalDate dataInicio = LocalDate.parse(campanhaDTO.getDataInicioVigencia(), DateTimeFormatter.ofPattern("d/MM/yyyy"));
			LocalDate dataFim = LocalDate.parse(campanhaDTO.getDataFimVigencia(), DateTimeFormatter.ofPattern("d/MM/yyyy"));
			
			if(dataInicio.isBefore(LocalDate.now())) {
				throw new DesafioException("Falha ao cadastrar a campanha. Data inicial menor que a data corrente");
			}
			
			if(dataInicio.isAfter(dataFim)) {
				throw new DesafioException("Falha ao cadastrar a campanha. Data inicial maior que data final");
			}
	
		} catch(DateTimeParseException e) {
			throw new DesafioException("Falha ao cadastrar a campanha. Formato de data inválido. ex: 10/10/2016");	
		}
	}
	
	public static void validaDadosEntradaSocioTorcedor(SocioTorcedorDTO socioTorcedorDTO) throws DesafioException {
		
		if(socioTorcedorDTO == null)
			throw new DesafioException("Falha ao cadastrar o sócio torcedor. Sócio não preenchid0");
		
		if(StringUtils.isBlank(socioTorcedorDTO.getNome()))
			throw new DesafioException("Falha ao cadastrar o sócio torcedor. Nome não preenchido");
		
		if(StringUtils.isBlank(socioTorcedorDTO.getEmail()))
			throw new DesafioException("Falha ao cadastrar o sócio torcedor. Email não preenchido");
		
		if(socioTorcedorDTO.getIdTime() == null)
			throw new DesafioException("Falha ao cadastrar o sócio torcedor. Id do time não preenchido");
		
		if(StringUtils.isBlank(socioTorcedorDTO.getDataNascimento()))
			throw new DesafioException("Falha ao cadastrar o sócio torcedor. Data de nascimento não preenchida");
		
		
		try {
			LocalDate dataInicio = LocalDate.parse(socioTorcedorDTO.getDataNascimento(), DateTimeFormatter.ofPattern("d/MM/yyyy"));
			
	
		} catch(DateTimeParseException e) {
			throw new DesafioException("Falha ao cadastrar o sócio torcedor. Formato de data inválido. ex: 10/10/2016");	
		}
	}
}
