package br.com.appsoftware.desafio.handler;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import br.com.appsoftware.desafio.dto.MensagemErroDTO;
import br.com.appsoftware.desafio.exception.DesafioException;

@Provider
public class DesafioExceptionHandler implements ExceptionMapper<DesafioException> {

	@Override
	public Response toResponse(DesafioException exception) {
		return buildResponse(exception.getMessage());
	}
	
	protected Response buildResponse(Object entity) {

        ResponseBuilder builder = Response.status(Status.BAD_REQUEST).entity(
        		new MensagemErroDTO(entity.toString()));
        builder.type(MediaType.APPLICATION_JSON);
        return builder.build();
    }
}	
