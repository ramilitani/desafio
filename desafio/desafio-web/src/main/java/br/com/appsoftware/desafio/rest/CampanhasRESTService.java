package br.com.appsoftware.desafio.rest;

import java.util.List;

import javax.faces.bean.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import br.com.appsoftware.desafio.dto.CampanhaDTO;
import br.com.appsoftware.desafio.exception.DesafioException;
import br.com.appsoftware.desafio.service.CampanhaService;
import br.com.appsoftware.desafio.util.ValidaDadosUtil;

@Path("/campanhas")
@RequestScoped
public class CampanhasRESTService {
	
	@Inject
	CampanhaService campanhaService;
	
	@GET
	@Path("{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response buscaCampanha(@PathParam("id") Long id) throws DesafioException {
		
		CampanhaDTO campanhaDTO = campanhaService.buscaCampanha(id);
		return Response.ok().entity(campanhaDTO).build();
	}
	
	@POST
    @Produces(MediaType.APPLICATION_JSON)
	public Response incluiCampanha(CampanhaDTO campanhaDTO) throws DesafioException {
		
    	ValidaDadosUtil.validaDadosEntradaCampanha(campanhaDTO);
    	List<CampanhaDTO> campanhas = campanhaService.criaCampanha(campanhaDTO);
		return Response.ok().entity(campanhas).build();
	}
	
	@PUT
	@Produces(MediaType.APPLICATION_JSON)
	public Response alteraCampanha(CampanhaDTO campanhaDTO) throws DesafioException {
		
		ValidaDadosUtil.validaDadosEntradaCampanha(campanhaDTO);
		campanhaService.alteraCampanha(campanhaDTO);
		return Response.ok().entity("alteração efetuada com sucesso.").build();
	}
	
	@DELETE
	@Path("{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response excluiCampanha(@PathParam("id") Long id) throws DesafioException {
		campanhaService.excluiCampanha(id);
		return Response.ok().entity("exclusão com sucesso.").build();
	}
}