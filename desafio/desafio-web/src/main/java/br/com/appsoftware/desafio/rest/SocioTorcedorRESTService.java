package br.com.appsoftware.desafio.rest;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import br.com.appsoftware.desafio.dto.SocioTorcedorDTO;
import br.com.appsoftware.desafio.exception.DesafioException;
import br.com.appsoftware.desafio.service.SocioTorcedorService;
import br.com.appsoftware.desafio.util.ValidaDadosUtil;

@Path("/sociotorcedor")
@RequestScoped
public class SocioTorcedorRESTService {
	
	@Inject
	SocioTorcedorService socioTorcedorService;
	
	@POST
    @Produces(MediaType.APPLICATION_JSON)
	public Response cadastraSocioTorcedor(SocioTorcedorDTO socioTorcedorDTO) throws DesafioException {
		
		ValidaDadosUtil.validaDadosEntradaSocioTorcedor(socioTorcedorDTO);
		socioTorcedorDTO = socioTorcedorService.cadastraSocioTorcedor(socioTorcedorDTO);
		return Response.ok().entity(socioTorcedorDTO).build();
	}
}
