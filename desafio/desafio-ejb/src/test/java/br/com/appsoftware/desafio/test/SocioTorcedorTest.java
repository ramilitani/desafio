package br.com.appsoftware.desafio.test;

import static org.junit.Assert.*;

import javax.inject.Inject;
import javax.swing.plaf.synth.SynthSeparatorUI;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.Archive;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.junit.Test;
import org.junit.internal.runners.statements.Fail;
import org.junit.runner.RunWith;

import br.com.appsoftware.desafio.dao.CampanhaDAO;
import br.com.appsoftware.desafio.dao.SocioTorcedorDAO;
import br.com.appsoftware.desafio.dao.TimeDAO;
import br.com.appsoftware.desafio.dto.SocioTorcedorDTO;
import br.com.appsoftware.desafio.entity.Campanha;
import br.com.appsoftware.desafio.entity.SocioTorcedor;
import br.com.appsoftware.desafio.entity.Time;
import br.com.appsoftware.desafio.exception.DesafioException;
import br.com.appsoftware.desafio.service.SocioTorcedorService;
import br.com.appsoftware.desafio.util.ParserUtil;
import br.com.appsoftware.desafio.util.Resources;

@RunWith(Arquillian.class)
public class SocioTorcedorTest {
	
	@Deployment
    public static Archive<?> createTestArchive() {       
        return ShrinkWrap.create(WebArchive.class, "test.war")
                .addClasses(DesafioException.class,SocioTorcedorService.class, SocioTorcedorDAO.class, SocioTorcedorDTO.class,
                		SocioTorcedor.class,TimeDAO.class,CampanhaDAO.class, Time.class, Campanha.class, ParserUtil.class, Resources.class)
                .addAsResource("META-INF/test-persistence.xml", "META-INF/persistence.xml")
                .addAsWebInfResource(EmptyAsset.INSTANCE, "beans.xml");
	}
	
	@Inject
	SocioTorcedorService socioTorcedorService;
	
	@Test
	public void cadastrarSocioTorcedor() {
		SocioTorcedorDTO socioTorcedorDTO = new SocioTorcedorDTO();
		socioTorcedorDTO.setNome("Joao");
		socioTorcedorDTO.setEmail("joao@gmail.com");
		socioTorcedorDTO.setDataNascimento("10/10/1998");
		socioTorcedorDTO.setIdTime(new Long(1));
		
		try {
			socioTorcedorDTO = socioTorcedorService.cadastraSocioTorcedor(socioTorcedorDTO);
			
		} catch (DesafioException e) {
			e.printStackTrace();
		}
		
		System.out.println(socioTorcedorDTO.getId());
		assertNotNull(socioTorcedorDTO.getId());	
	}
	
	@Test
	public void retornaMensagemDeCadastroJaEfetuado() {
		SocioTorcedorDTO socioTorcedorDTO = new SocioTorcedorDTO();
		socioTorcedorDTO.setNome("Joao");
		socioTorcedorDTO.setEmail("joao@gmail.com");
		socioTorcedorDTO.setDataNascimento("10/10/1998");
		socioTorcedorDTO.setIdTime(new Long(1));
		
		try {
			socioTorcedorDTO = socioTorcedorService.cadastraSocioTorcedor(socioTorcedorDTO);
			fail("Deveria retornar uma exception");
		} catch (DesafioException e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void retornaListaDeCampanhasDoSocioTorcedorSemCampanhaAssociada() {
		SocioTorcedorDTO socioTorcedorDTO = new SocioTorcedorDTO();
		socioTorcedorDTO.setNome("Joao");
		socioTorcedorDTO.setEmail("joao@gmail.com");
		socioTorcedorDTO.setDataNascimento("10/10/1998");
		socioTorcedorDTO.setIdTime(new Long(1));
		
		try {
			socioTorcedorDTO = socioTorcedorService.cadastraSocioTorcedor(socioTorcedorDTO);
			
		} catch (DesafioException e) {
			
			assertTrue(socioTorcedorDTO.getCampanhas() != null && socioTorcedorDTO.getCampanhas().size() > 1);
			
			if(socioTorcedorDTO.getCampanhas() == null || socioTorcedorDTO.getCampanhas().isEmpty()) {
				fail("Deveria retornar lista de campanhas");
			}
		}
	}
}
