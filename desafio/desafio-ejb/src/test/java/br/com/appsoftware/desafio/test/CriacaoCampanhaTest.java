package br.com.appsoftware.desafio.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import javax.inject.Inject;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.Archive;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.junit.Test;
import org.junit.runner.RunWith;

import br.com.appsoftware.desafio.dao.CampanhaDAO;
import br.com.appsoftware.desafio.dao.TimeDAO;
import br.com.appsoftware.desafio.dto.CampanhaDTO;
import br.com.appsoftware.desafio.entity.Campanha;
import br.com.appsoftware.desafio.entity.Time;
import br.com.appsoftware.desafio.exception.DesafioException;
import br.com.appsoftware.desafio.service.CampanhaService;
import br.com.appsoftware.desafio.util.ParserUtil;
import br.com.appsoftware.desafio.util.Resources;

@RunWith(Arquillian.class)
public class CriacaoCampanhaTest {
	
	@Deployment
    public static Archive<?> createTestArchive() {       
        return ShrinkWrap.create(WebArchive.class, "test3.war")
                .addClasses(DesafioException.class, CampanhaDTO.class,Campanha.class,Time.class, CampanhaDAO.class, 
                		CampanhaService.class, Resources.class, ParserUtil.class, TimeDAO.class)
                .addAsResource("META-INF/test-persistence.xml", "META-INF/persistence.xml")
                .addAsWebInfResource(EmptyAsset.INSTANCE, "beans.xml");
	}
	
	@Inject
	CampanhaService campanhaService;
	
	@Test
	public void criaCampanha() throws DesafioException {
	
		CampanhaDTO campanhaDTO = new CampanhaDTO();
		campanhaDTO.setNome("teste 1");
		campanhaDTO.setIdTime(new Long(1));
		campanhaDTO.setDataInicioVigencia("06/05/2017");
		campanhaDTO.setDataFimVigencia("06/05/2017");
		//campanhaDTO = campanhaService.criaCampanha(campanhaDTO);
		System.out.println("id campanha" + campanhaDTO.getId());
		assertNotNull(campanhaDTO.getId());
	}
}
