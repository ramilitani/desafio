package br.com.appsoftware.desafio.dao;

import java.time.LocalDate;
import java.util.List;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import br.com.appsoftware.desafio.entity.Campanha;
import br.com.appsoftware.desafio.entity.Time;

public class CampanhaDAO {
	
	@Inject
	@PersistenceContext(name="primary")
	EntityManager em;
	
	public void cadastra(Campanha campanha) throws Exception {
		em.persist(campanha);
	}
	
	public void atualiza(Campanha campanha) throws Exception {
		em.merge(campanha);
		em.flush();
	}

	@SuppressWarnings("unchecked")
	public List<Campanha> listaCampanhas() {		
		Query query = em.createQuery("SELECT c FROM Campanha c WHERE c.dataFimVigencia >= CURRENT_DATE");
		return query.getResultList();
	}
	
	@SuppressWarnings("unchecked")
	public List<Campanha> listaCampanhasPorTime(Time timeDoCoracao) {
		Query query = em.createQuery("SELECT c FROM Campanha c WHERE c.time.id = :timeDoCoracao AND c.dataFimVigencia >= CURRENT_DATE");
		query.setParameter("timeDoCoracao", timeDoCoracao.getId());
		
		return query.getResultList();
	} 
	
  	public boolean existeCampanhasAssociadas(Long idCampanha, LocalDate dataFimVigencia, boolean isAtualizacao) {
  		Query query = null;
  		
  		if(isAtualizacao) {
  			query = em.createQuery("SELECT COUNT(c) FROM Campanha c WHERE c.dataFimVigencia = :dataFimVigencia AND c.id != :idCampanha");
  			query.setParameter("idCampanha", idCampanha);
  		}else {
  			query = em.createQuery("SELECT COUNT(c) FROM Campanha c WHERE c.dataFimVigencia = :dataFimVigencia");
  		}
		
		query.setParameter("dataFimVigencia", dataFimVigencia);
		
		long quantidade = (Long) query.getSingleResult();
		
		if(quantidade > 0) {
			return true;
		}
		
		return false;
	}

	public void remove(Campanha campanha) {
		em.remove(campanha);
		
	}

	public Campanha busca(Long idCampanha) {
		return em.find(Campanha.class, idCampanha);
	}	
}  