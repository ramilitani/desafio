package br.com.appsoftware.desafio.util;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import br.com.appsoftware.desafio.dto.CampanhaDTO;
import br.com.appsoftware.desafio.dto.SocioTorcedorDTO;
import br.com.appsoftware.desafio.entity.Campanha;
import br.com.appsoftware.desafio.entity.SocioTorcedor;

public final class ParserUtil {
	
	private static DateTimeFormatter formatador = DateTimeFormatter.ofPattern("dd/MM/yyyy");
	
	public static Campanha convertCampanhaDtoToCampanha(CampanhaDTO campanhaDTO) {
		Campanha campanha = new Campanha();
		campanha.setNome(campanhaDTO.getNome());
		campanha.setDataInicioVigencia(LocalDate.parse(campanhaDTO.getDataInicioVigencia(), formatador));
		campanha.setDataFimVigencia(LocalDate.parse(campanhaDTO.getDataFimVigencia(), formatador));
		campanha.setDataCadastro(LocalDateTime.now());
		return campanha;	
	}
	
	public static CampanhaDTO convertCampanhaToCampanhaDTO(Campanha campanha) {
		CampanhaDTO campanhaDTO = new CampanhaDTO();
		campanhaDTO.setId(campanha.getId());
		campanhaDTO.setNome(campanha.getNome());
		campanhaDTO.setIdTime(campanha.getTime().getId());
		campanhaDTO.setDataInicioVigencia(campanha.getDataInicioVigencia().format(formatador));
		campanhaDTO.setDataFimVigencia(campanha.getDataFimVigencia().format(formatador));
		return campanhaDTO;
		
	}
	
	public static List<CampanhaDTO> convertCampanhasToCampanhasDTO(List<Campanha> campanhas) {
		
		List<CampanhaDTO> campanhasDTO = null;
		
		if(campanhas != null && campanhas.size() > 0) {
			campanhasDTO = new ArrayList<CampanhaDTO>();
			for(Campanha campanha : campanhas) {
				campanhasDTO.add(convertCampanhaToCampanhaDTO(campanha));
			}
		}
		
		return campanhasDTO;
	}

	public static SocioTorcedor converterSocioTorcedorDtoToSocioTorcedor(SocioTorcedorDTO socioTorcedorDTO) {
		SocioTorcedor socioTorcedor = new SocioTorcedor();
		socioTorcedor.setNome(socioTorcedorDTO.getNome());
		socioTorcedor.setEmail(socioTorcedorDTO.getEmail());
		socioTorcedor.setDataNascimento(LocalDate.parse(socioTorcedorDTO.getDataNascimento(), formatador));
		socioTorcedor.setDataCadastro(LocalDateTime.now());
		return socioTorcedor;
	}
	
	public static LocalDate convertStringToLocalDate(String data) {
		return LocalDate.parse(data, formatador);
	}

}
