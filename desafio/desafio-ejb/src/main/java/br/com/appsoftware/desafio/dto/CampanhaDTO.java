package br.com.appsoftware.desafio.dto;

public class CampanhaDTO {
	
	private Long id;
	private String nome;
	private Long idTime;
	private String dataInicioVigencia;
	private String dataFimVigencia;
	
	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}
	
	public String getNome() {
		return nome;
	}
	
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	public Long getIdTime() {
		return idTime;
	}
	public void setIdTime(Long idTime) {
		this.idTime = idTime;
	}
	
	public String getDataInicioVigencia() {
		return dataInicioVigencia;
	}
	
	public void setDataInicioVigencia(String dataInicioVigencia) {
		this.dataInicioVigencia = dataInicioVigencia;
	}
	
	public String getDataFimVigencia() {
		return dataFimVigencia;
	}
	
	public void setDataFimVigencia(String dataFimVigencia) {
		this.dataFimVigencia = dataFimVigencia;
	}
}
