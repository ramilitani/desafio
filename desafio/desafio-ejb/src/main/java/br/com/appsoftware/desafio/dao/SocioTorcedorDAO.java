package br.com.appsoftware.desafio.dao;

import java.util.List;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import br.com.appsoftware.desafio.entity.SocioTorcedor;

public class SocioTorcedorDAO {
	
	@Inject
	@PersistenceContext(name="primary")
	EntityManager em;

	public void cadastra(SocioTorcedor socioTorcedor) {
		em.persist(socioTorcedor);		
	}

	public SocioTorcedor buscaPorEmail(String email) {
		SocioTorcedor socioTorcedor = null;
		Query query = em.createQuery("SELECT s FROM SocioTorcedor s WHERE s.email = :email");
		query.setParameter("email", email);
		
		try {
			socioTorcedor = (SocioTorcedor) query.getSingleResult();
		} catch(NoResultException e) {}
				
		return socioTorcedor;
	}

	public List<SocioTorcedor> buscarSociosPorCampanha(Long idCampanha) {
		Query query = em.createQuery("SELECT s FROM SocioTorcedor s JOIN FETCH s.campanhas c WHERE c.id = :idCampanha");
		query.setParameter("idCampanha", idCampanha);
	
		return query.getResultList();
	}

	public void atualizar(SocioTorcedor socioTorcedor) {
		em.merge(socioTorcedor);
	}
}
