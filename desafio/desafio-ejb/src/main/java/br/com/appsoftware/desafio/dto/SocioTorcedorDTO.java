package br.com.appsoftware.desafio.dto;

import java.util.List;

public class SocioTorcedorDTO {
	
	private Long id;
	private String nome;
	private String email;
	private String dataNascimento;
	private Long idTime;
	private List<CampanhaDTO> campanhas;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}
	
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	public String getEmail() {
		return email;
	}
	
	public void setEmail(String email) {
		this.email = email;
	}
	
	public String getDataNascimento() {
		return dataNascimento;
	}
	
	public void setDataNascimento(String dataNascimento) {
		this.dataNascimento = dataNascimento;
	}
	
	public Long getIdTime() {
		return idTime;
	}
	
	public void setIdTime(Long idTime) {
		this.idTime = idTime;
	}

	public List<CampanhaDTO> getCampanhas() {
		return campanhas;
	}

	public void setCampanhas(List<CampanhaDTO> campanhas) {
		this.campanhas = campanhas;
	}	
}
