package br.com.appsoftware.desafio.dto;

public class MensagemErroDTO {
	
	private String erro;
	
	public MensagemErroDTO(String erro) {
		this.erro = erro;
	}
	
	public String getErro() {
		return erro;
	}

	public void setErro(String erro) {
		this.erro = erro;
	}
}
