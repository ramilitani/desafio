package br.com.appsoftware.desafio.exception;

import java.io.Serializable;

public class DesafioException extends Exception implements Serializable {

	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public DesafioException(String msg) {
		super(msg);
	}
	
	public DesafioException(String msg, Exception e) {
		super(msg, e);
	}

}
