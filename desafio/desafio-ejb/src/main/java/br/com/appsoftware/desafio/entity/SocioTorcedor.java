package br.com.appsoftware.desafio.entity;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Email;

@Entity
public class SocioTorcedor implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -3763770843594471361L;
	
	private Long id;
	private String nome;
	private String email;
	private LocalDate dataNascimento;
	private Time timeDoCoracao;
	private LocalDateTime dataCadastro;
	private List<Campanha> campanhas;
	
	@Id
	@GeneratedValue
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	@NotNull
	public String getNome() {
		return nome;
	}
	
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	@Email
	public String getEmail() {
		return email;
	}
	
	public void setEmail(String email) {
		this.email = email;
	}
	
	@Column(name="data_nascimento")
	public LocalDate getDataNascimento() {
		return dataNascimento;
	}
	
	public void setDataNascimento(LocalDate dataNascimento) {
		this.dataNascimento = dataNascimento;
	}
	
	@NotNull
	@ManyToOne
	@JoinColumn(name="time_id")
	public Time getTimeDoCoracao() {
		return timeDoCoracao;
	}
	
	public void setTimeDoCoracao(Time timeDoCoracao) {
		this.timeDoCoracao = timeDoCoracao;
	}
	
	@ManyToMany(cascade=CascadeType.ALL)
	@JoinTable(name="socio_campanhas", joinColumns=
	{@JoinColumn(name="socio_id")}, inverseJoinColumns=
	{@JoinColumn(name="campanha_id")} )
	public List<Campanha> getCampanhas() {
		return campanhas;
	}

	public void setCampanhas(List<Campanha> campanhas) {
		this.campanhas = campanhas;
	}
	
	
	@Column(name="data_cadastro")
	public LocalDateTime getDataCadastro() {
		return dataCadastro;
	}

	public void setDataCadastro(LocalDateTime dataCadastro) {
		this.dataCadastro = dataCadastro;
	}	
}
