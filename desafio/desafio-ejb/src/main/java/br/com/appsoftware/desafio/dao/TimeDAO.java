package br.com.appsoftware.desafio.dao;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import br.com.appsoftware.desafio.entity.Time;

public class TimeDAO {
	
	@Inject
	@PersistenceContext(name="primary") 
	EntityManager em;
	
	public Time obterTime(Long id) throws Exception {
		return em.find(Time.class, id);
	}
}
