package br.com.appsoftware.desafio.service;

import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.inject.Inject;

import br.com.appsoftware.desafio.dao.CampanhaDAO;
import br.com.appsoftware.desafio.dao.SocioTorcedorDAO;
import br.com.appsoftware.desafio.dao.TimeDAO;
import br.com.appsoftware.desafio.dto.CampanhaDTO;
import br.com.appsoftware.desafio.entity.Campanha;
import br.com.appsoftware.desafio.entity.SocioTorcedor;
import br.com.appsoftware.desafio.entity.Time;
import br.com.appsoftware.desafio.exception.DesafioException;
import br.com.appsoftware.desafio.util.ParserUtil;

@Stateless
@TransactionManagement(TransactionManagementType.CONTAINER)
public class CampanhaService {
	
	@Inject
	CampanhaDAO campanhaDAO;
	
	@Inject
	SocioTorcedorDAO socioTorcedorDAO;
	
	@Inject
	TimeDAO timeDAO;
	
	public CampanhaDTO buscaCampanha(Long idCampanha) throws DesafioException {
		
		Campanha campanha = campanhaDAO.busca(idCampanha);

		if(campanha == null) {
			throw new DesafioException("A campanha não foi identificada");
		}
		
		try {
			return ParserUtil.convertCampanhaToCampanhaDTO(campanha);
		} catch(Exception e) {
			throw new DesafioException("Falha ao buscar campanha. Erro: " + e.getMessage());
		} 
	}
	
	public List<CampanhaDTO> criaCampanha(CampanhaDTO campanhaDTO) throws DesafioException {
		
		valida(campanhaDTO, false);
		
		try {
			Campanha campanha = ParserUtil.convertCampanhaDtoToCampanha(campanhaDTO);
			Time time = timeDAO.obterTime(campanhaDTO.getIdTime());
			campanha.setTime(time);
			
			verificaVigencia(null, campanhaDTO.getDataFimVigencia());
			campanhaDAO.cadastra(campanha);
			
		} catch (Exception e) {
			throw new DesafioException("Falha ao cadastrar campanha. Erro: " + e.getMessage());
		}
		
		return listaCampanhas();
	}
	
	public void alteraCampanha(CampanhaDTO campanhaDTO) throws DesafioException {
		
		valida(campanhaDTO, true);
		
		try {
			Campanha campanha = campanhaDAO.busca(campanhaDTO.getId());
			Time time = timeDAO.obterTime(campanhaDTO.getIdTime());
			
			verificaVigencia(campanhaDTO.getId(), campanhaDTO.getDataFimVigencia());
			
			campanha.setNome(campanhaDTO.getNome());
			campanha.setDataInicioVigencia(ParserUtil.convertStringToLocalDate(campanhaDTO.getDataInicioVigencia()));
			campanha.setDataFimVigencia(ParserUtil.convertStringToLocalDate(campanhaDTO.getDataFimVigencia()));
			campanha.setTime(time);
			campanhaDAO.atualiza(campanha);
		
		} catch(Exception e) {
			throw new DesafioException(e.getMessage());
		}	
	}
	
	public void excluiCampanha(Long idCampanha) throws DesafioException {
		
		Campanha campanha = campanhaDAO.busca(idCampanha);
		
		if(campanha == null) {
			throw new DesafioException("A campanha não foi identificada");
		}
		
		try {
			List<SocioTorcedor> sociosTorcedores = socioTorcedorDAO.buscarSociosPorCampanha(idCampanha);
			
			if(sociosTorcedores != null && !sociosTorcedores.isEmpty()) {
				
				for(SocioTorcedor socioTorcedor : sociosTorcedores) {
					
					for(int i = 0; i < socioTorcedor.getCampanhas().size(); i++) {
						Campanha campanhaSocioTorcedor = socioTorcedor.getCampanhas().get(i);
						socioTorcedor.getCampanhas().remove(campanhaSocioTorcedor);
						socioTorcedorDAO.atualizar(socioTorcedor);
						break;
					}
				}
			}
			
			campanhaDAO.remove(campanha);
			
		} catch(Exception e) {
			throw new DesafioException("Falha ao remover campanha." + e.getMessage());
		}
	}
	
	
	private List<CampanhaDTO> listaCampanhas() throws DesafioException{
		
		try {
			return ParserUtil.convertCampanhasToCampanhasDTO(campanhaDAO.listaCampanhas());
		} catch(Exception e) {
			throw new DesafioException("Falha ao cadastrar campanha. Erro: " + e.getMessage());
		}
	}
	
	private void verificaVigencia(Long idCampanha, String dataFimVigencia) throws DesafioException{
		boolean existeCampanhasAssociadas = false;
		
		if(idCampanha == null) {
			
			do {
				existeCampanhasAssociadas = campanhaDAO.existeCampanhasAssociadas(null, ParserUtil.convertStringToLocalDate(dataFimVigencia), false);
				if(existeCampanhasAssociadas) {
					aumentaFimDaVigencia();
				}
			} while(existeCampanhasAssociadas);
			
		}else {
			
			do {
				existeCampanhasAssociadas = campanhaDAO.existeCampanhasAssociadas(idCampanha, ParserUtil.convertStringToLocalDate(dataFimVigencia), true);
				if(existeCampanhasAssociadas) {
					aumentaFimDaVigencia();
				}
			} while(existeCampanhasAssociadas);
		}
	}
	
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	private void aumentaFimDaVigencia() throws DesafioException {
		
		List<Campanha> campanhas = campanhaDAO.listaCampanhas();
		
		for(Campanha campanha : campanhas) {
			campanha.setDataFimVigencia(campanha.getDataFimVigencia().plusDays(1));
			
			try {
				campanhaDAO.atualiza(campanha);
			} catch (Exception e) {
				throw new DesafioException("Falha ao cadastrar campanha. Erro: " + e.getMessage());
			}
		}
	}
	
	public void valida(CampanhaDTO campanhaDTO, boolean isAtualizacao) throws DesafioException {
		
		try {
			
			if(isAtualizacao) {
				if(campanhaDTO.getId() == null && isAtualizacao) {
					throw new DesafioException("Id da campanha não foi informado");
				}

				Campanha campanha = campanhaDAO.busca(campanhaDTO.getId());
				
				if(campanha == null) {
					throw new DesafioException("A campanha não foi identificada");
				}
			}
			
			Time time = timeDAO.obterTime(campanhaDTO.getIdTime());
			
			if(time == null) {
				throw new DesafioException("Time informado não existe.");
			}
		} catch(Exception e) {
			throw new DesafioException(e.getMessage());
		}	
	}
}
