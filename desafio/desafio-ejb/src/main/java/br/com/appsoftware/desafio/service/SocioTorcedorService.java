package br.com.appsoftware.desafio.service;

import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.inject.Inject;

import br.com.appsoftware.desafio.dao.CampanhaDAO;
import br.com.appsoftware.desafio.dao.SocioTorcedorDAO;
import br.com.appsoftware.desafio.dao.TimeDAO;
import br.com.appsoftware.desafio.dto.SocioTorcedorDTO;
import br.com.appsoftware.desafio.entity.Campanha;
import br.com.appsoftware.desafio.entity.SocioTorcedor;
import br.com.appsoftware.desafio.entity.Time;
import br.com.appsoftware.desafio.exception.DesafioException;
import br.com.appsoftware.desafio.util.ParserUtil;

@Stateless
@TransactionManagement(TransactionManagementType.CONTAINER)
public class SocioTorcedorService {
	
	@Inject
	SocioTorcedorDAO socioTorcedorDAO;
	
	@Inject
	TimeDAO timeDAO;
	
	@Inject
	CampanhaDAO campanhaDAO;
	
	public SocioTorcedorDTO cadastraSocioTorcedor(SocioTorcedorDTO socioTorcedorDTO) throws DesafioException {
		
		try {			
			SocioTorcedor socioTorcedor = socioTorcedorDAO.buscaPorEmail(socioTorcedorDTO.getEmail());
			boolean naoPossuiCampanhasAssociadas = true;
			
			if(socioTorcedor != null){
				naoPossuiCampanhasAssociadas = socioTorcedor.getCampanhas() == null || socioTorcedor.getCampanhas().isEmpty();
				
				if(!naoPossuiCampanhasAssociadas) {
					throw new Exception("Cadastro já foi efetuado anteriormente");
				}
			}else {
				socioTorcedor = ParserUtil.converterSocioTorcedorDtoToSocioTorcedor(socioTorcedorDTO);
				Time time = timeDAO.obterTime(socioTorcedorDTO.getIdTime());
				socioTorcedor.setTimeDoCoracao(time);
				socioTorcedorDAO.cadastra(socioTorcedor);
				socioTorcedorDTO.setId(socioTorcedor.getId());
			}
			
			if(naoPossuiCampanhasAssociadas) {
				List<Campanha> campanhas = campanhaDAO.listaCampanhasPorTime(socioTorcedor.getTimeDoCoracao());
				
				if(campanhas != null && !campanhas.isEmpty()) {
					socioTorcedorDTO.setCampanhas(ParserUtil.convertCampanhasToCampanhasDTO(campanhas));
				}
			}
		} catch(Exception e) {
			throw new DesafioException(e.getMessage());
		}
		
		return socioTorcedorDTO;
	}

}
